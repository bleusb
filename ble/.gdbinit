define hook-quit
  set confirm off
end

# print demangled symbols by default
set print asm-demangle on

# OpenOCD
set remote hardware-breakpoint-limit 6
set remote hardware-watchpoint-limit 4
target extended-remote :3333
#target remote :3333
#monitor flash breakpoints 1
monitor arm semihosting enable
monitor arm semihosting_fileio enable
#monitor arm semihosting IOClient 3
