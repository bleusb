use trinket_m0::{
    clock::GenericClockController,
    gpio::{self, Floating, Input},
    pac::{PM, SERCOM1},
    prelude::*,
    sercom::{self, PadPin, SPIMaster1},
};

use apa102_spi::Apa102;

type TrinketDotStar = Apa102<
    SPIMaster1<
        sercom::Sercom1Pad3<gpio::Pa31<gpio::PfD>>,
        sercom::Sercom1Pad0<gpio::Pa0<gpio::PfD>>,
        sercom::Sercom1Pad1<gpio::Pa1<gpio::PfD>>,
    >,
>;

pub fn new(
    sercom: SERCOM1,
    miso: gpio::Pa31<Input<Floating>>,
    mosi: gpio::Pa0<Input<Floating>>,
    sck: gpio::Pa1<Input<Floating>>,
    port: &mut gpio::Port,
    pm: &mut PM,
    clocks: &mut GenericClockController,
) -> TrinketDotStar {
    let gclk = clocks.gclk0();
    let miso = miso.into_pad(port);
    let mosi = mosi.into_pad(port);
    let sck = sck.into_pad(port);
    let spi = SPIMaster1::new(
        &clocks
            .sercom1_core(&gclk)
            .expect("setting up sercom1 clock"),
        3.mhz(),
        apa102_spi::MODE,
        sercom,
        pm,
        (miso, mosi, sck),
    );

    Apa102::new(spi)
}
